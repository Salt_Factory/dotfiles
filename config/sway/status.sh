# The Sway configuration file in ~/.config/sway/config calls this script.
# You should see changes to the status bar after saving this script.
# If not, do "killall swaybar" and $mod+Shift+c to reload the configuration.

# The abbreviated weekday (e.g., "Sat"), followed by the ISO-formatted date
# like 2018-10-06 and the time (e.g., 14:01)
date_formatted=$(date "+%a %F %H:%M")

pomodoro_time=$(pomodoro clock)


current_artist=''
current_title=''
cmus_playing=''
ncspot_playing=''
if cmus-remote -C ;
then
    # If cmus is active, get the information and display it.
    # cmus information
    current_artist=$(cmus-remote -Q | grep "\sartist\s" | cut -d " " -f 3-)
    current_title=$(cmus-remote -Q | grep "\stitle\s" | cut -d " " -f 3-)
    cmus_playing=$(cmus-remote -Q | grep "status\s" | cut -d " " -f 2-)
fi

if nc -W 1 -U ~/.cache/ncspot/ncspot.sock > /dev/null;
then
    # ncspot outputs json through the socket -- use jq to grab the first
    # artist and the title of the current song.
    # We use `eval` to strip the quotation marks around the output.
    ncspot_info=$(nc -W 1 -U ~/.cache/ncspot/ncspot.sock);
    eval current_artist=$(echo $ncspot_info | jq '.playable.artists[0]')
    eval current_title=$(echo $ncspot_info | jq '.playable.title')
    eval ncspot_playing=$(echo $ncspot_info | jq '.mode.Playing.secs_since_epoch');
fi

if [ $ncspot_playing != null ] || [ $cmus_playing = 'playing' ];
then
    playing='▶';
else
    playing='⏸';
fi

if [ "$current_artist" = "" ];
then
    echo $pomodoro_time '|' $date_formatted;
else
    echo $playing $current_artist - $current_title ♫ '|' $pomodoro_time '|' $date_formatted;
    #echo $cmus_playing
fi
