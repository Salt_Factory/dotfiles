#!/bin/bash

# Simple bash script to connect my bluetooth headphones.
# The cheap bluetooth connector I'm using doesn't allow for automatic connection.
# On top of this, it tends to connect in the wrong sink mode. This script works every time!
# Author: SaltFactory (Simon Vandevelde)

bluetoothctl power on
bluetoothctl agent on
sleep 3

bluetoothctl connect 00:08:E0:72:85:89
