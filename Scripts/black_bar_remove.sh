# Author: Simon Vandevelde
# Simple script to remove black bars using FFmpeg
# Only removes black bars from the bottom and from the right of the screen, as this is how I usually record using OBS (with the video in the top-left corner)

echo "Removing black bars from "$1""

cropdetect=$(ffmpeg -ss 1 -i "$1" -vframes 2 -vf cropdetect -f null - 2>&1 | grep crop | awk '{printf "%s\n%s",$5,$7}' | awk -F':' '{print $2}')

echo $cropdetect
width=$(echo $cropdetect | awk -F' ' '{print $1}')
height=$(echo $cropdetect | awk -F' ' '{print $2}')
echo $width $height

ffmpeg -i "$1" -vf crop=$width:$height:0:0 -c:a copy $2
