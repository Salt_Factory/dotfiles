#!/bin/bash
# Script to pull my work folder from the backupserver.
# Setup of SSH keys: https://kb.iu.edu/d/aews .
# Setup of local and remote backup repos: https://restic.readthedocs.io/en/stable/030_preparing_a_new_repo.html#sftp

# Set these variables.
work_path_in_backup=/home/sva/Documents/Doctoraat
work_path_local=/home/saltfactory/Documents/Doctoraat
tmp_path=/home/saltfactory/.tmp/doctoraat
remote_host=10.64.50.69
remote_user=sva
remote_location=/repono/backup/sva/Restic


# Make a remote backup.
restic -r sftp:$remote_user@$remote_host:$remote_location restore latest --target $tmp_path --path $work_path_in_backup

# Delete previous work folder, and replace it by the new one.
find $work_path_local -name "*" -delete
mv $tmp_path$work_path_in_backup $work_path_local



