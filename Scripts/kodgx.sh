#!/bin/bash
# Script to pull my work folder from the backupserver.
# Setup of SSH keys: https://kb.iu.edu/d/aews .
# Setup of local and remote backup repos: https://restic.readthedocs.io/en/stable/030_preparing_a_new_repo.html#sftp

# Set these variables.
path_to_backup=
remote_host=
remote_user=
remote_location=


# Make a remote backup.
restic -r sftp:$remote_user@$remote_host:$remote_location backup $path_to_backup 

