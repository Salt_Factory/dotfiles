"vundle
set nocompatible

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

if has('python3')
endif

Plugin 'VundleVim/Vundle.vim'

" Advanced pandoc integration
Plugin 'vim-pandoc/vim-pandoc'

"Textx support
Plugin 'igordejanovic/textx.vim'
"Airline!
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
let g:airline_solarized_bg='dark'

" HTML and XML tag highlighting
Plugin 'valloric/matchtagalways'

" Plugin to help with XML files
Plugin 'sukima/xmledit'

" VimWiki + Vim-zettel
Plugin 'vimwiki/vimwiki'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
Plugin 'michal-h21/vim-zettel'

let g:vimwiki_list = [
    \ {
    \   'path': '~/.vimwiki/',
    \ }]

let g:zettel_format = "%y%m%d-%H%M-%title"
let g:zettel_options = [{"template" :  "~/.vimwiki/template.tpl"}]

"Linter with a lot of features, for all kinds of languages
Plugin 'w0rp/ale'

Plugin 'prabirshrestha/vim-lsp'


"auto-completion stuff
Plugin 'Valloric/YouCompleteMe'
Plugin 'ervandew/supertab'
Plugin 'tmhedberg/SimpylFold'

colorscheme desert

"Enhance latex experience by using vimtex
Plugin 'lervag/vimtex'
let g:tex_flavor = 'latex'

"Let the compiler compile a pdf on every savecommand
let g:vimtex_compiler_latexmk = {
    \ 'executable' : 'latexmk', 
    \ 'options' : [ 
        \   '-xelatex',
        \   '-shell-escape',
        \   '-file-line-error',
        \   '-synctex=1',
        \   '-interaction=nonstopmode',
        \ ],
    \}
" let g:vimtex_compiler_latexmk = {
"     \ 'executable' : 'latexmk', 
"     \ 'options' : [ 
"         \   '-xelatex',
"         \   '-shell-escape',
"         \   '-file-line-error',
"         \   '-synctex=1',
"         \   '-interaction=nonstopmode',
"         \ ],
"     \ 'build_dir' : './build',
"     \}

call vundle#end()

syntax on
filetype plugin indent on    " enables filetype detection
set nocompatible
let g:SimpylFold_docstring_preview = 1

"autocomplete
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_global_ycm_extra_conf = 'path to .ycm_extra_confg.py'

"custom keys
" let mapleader=" "
" map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
"
"I don't like swap files
set noswapfile

"turn on numbering
set nu

"omnicomplete
autocmd FileType python set omnifunc=pythoncomplete#Complete

" automatically generate Python docstrings!
Plugin 'heavenshell/vim-pydocstring'
let g:pydocstring_doq_path = '~/.local/bin/doq'

"------------Start Python PEP 8 stuff----------------
" Number of spaces that a pre-existing tab is equal to.
au BufRead,BufNewFile *py,*pyw,*.c,*.h set tabstop=4


"spaces for indents
set shiftwidth=4
set expandtab
set softtabstop=4

" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red

" Display tabs at the beginning of a line in Python mode as bad.
au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" Wrap text after a certain number of characters
au BufRead,BufNewFile *.py,*.pyw, set textwidth=79

" Use UNIX (\n) line endings.
au BufNewFile *.py,*.pyw,*.c,*.h set fileformat=unix

" Set the default file encoding to UTF-8:
set encoding=utf-8

" For full syntax highlighting in python:
let python_highlight_all=1

" Turn ofoldmethod=indent
set foldlevel=99
"use space to open folds
nnoremap <space> za 
"----------Stop python PEP 8 stuff--------------

" Activate syntaxfolding when opening c-files
" au BufRead, BufNewFile *.c,*.h set foldmethod=syntax
autocmd BufRead  *.c,*.h,*cpp setlocal foldmethod=syntax
"setlocal foldmethod=syntax
"js stuff"
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2

" autocmd BufWritePost *.rst :silent !bash compile.sh > /dev/null 2>&1


" Custom language server
let g:ycm_language_server = 
  \ [ 
  \   {
  \     'name': 'txt',
  \     'cmdline': [ '/home/saltfactory/Downloads/vscode-extension-samples/lsp-sample/', '--stdio' ],
  \     'filetypes': [ 'txt' ]
  \   },
  \ ]

au BufNewFile,BufRead *.idp set filetype=idp


" Manually set some linters for my commonly used languages.
" Every language not included will use the default set of available linters (check docs)
let g:ale_linters = {
            \   'python': ['flake8', 'mypy'],
            \   'tex': ['alex', 'lacheck', 'proselint', 'redpen'],
            \   'html': ['alex', 'proselint'],
            \   'restructuredtext': ['rstcheck', 'alex', 'redpen', 'proselint', 'write-good'],
            \   'idp': ['folint']
            \}
let g:ale_sign_column_always = 1
